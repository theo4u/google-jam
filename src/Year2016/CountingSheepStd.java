
package Year2016;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author THEOPHY
 */
public class CountingSheepStd {
    
    public static void main(String[] args){
       // int N;
         
         //java -classpath build\classes Year2016.CountingSheepStd <input.txt >output.txt
         Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
            int t = in.nextInt();  // Scanner has functions to read ints, longs, strings, chars, etc.
            for (int k = 1; k <= t; ++k) {
               int N = in.nextInt();
            
        int i=0;
        ArrayList<Integer> Numbers=new ArrayList<>();
        
        while(Numbers.size()<10){
            i++;
            int a=i*N;
           // System.out.println(i+"*"+N[k]+"="+a); debug purpose
            //process A
            String[] answer= (String.valueOf(a)).split("");//separate each number with '' after converting to string
            for(int j=0;j<answer.length;j++){
                //convert each string to integer back
              int n=Integer.parseInt(answer[j]);
                //checking the existense of each number in the answer if is in Numbers list
                if(Numbers.contains(n))
                    continue;
                else
                    Numbers.add(n);
            }
            //anticipate future for infinite loop
            if(a==((i+1)*N))
               break;
            
        }
         if((i*N)==((i+1)*N))
                System.out.println("Case #"+k+": INSOMNIA");
            else
              System.out.println("Case #"+k+":"+i*N);
        
      
        // showNumbers(Numbers); debugg purpose
         
        }//end of each case
    }
    
    
    private static void showNumbers(ArrayList<Integer> N){
        for(int i=0;i<N.size();i++)
        System.out.print(" "+i+":-"+N.get(i));
        System.out.println();
    }
}
