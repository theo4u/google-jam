
package Year2016;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author THEOPHY
 */
public class CountingSheep {
    
    public static void main(String[] args){
        int[] N;
         
         N=getInput("input.txt");
        for (int k=0;k<N.length;k++ ){//each case
            
        int i=0;
        ArrayList<Integer> Numbers=new ArrayList<>();
        
        while(Numbers.size()<10){
            i++;
            int a=i*N[k];
           // System.out.println(i+"*"+N[k]+"="+a); debug purpose
            //process A
            String[] answer= (String.valueOf(a)).split("");//separate each number with '' after converting to string
            for(int j=0;j<answer.length;j++){
                //convert each string to integer back
              int n=Integer.parseInt(answer[j]);
                //checking the existense of each number in the answer if is in Numbers list
                if(Numbers.contains(n))
                    continue;
                else
                    Numbers.add(n);
            }
            //anticipate future for infinite loop
            if(a==((i+1)*N[k]))
               break;
            
        }
         if((i*N[k])==((i+1)*N[k]))
                System.out.println("Case #"+(k+1)+": INSOMNIA");
            else
              System.out.println("Case #"+(k+1)+": "+i*N[k]);
        
      
        // showNumbers(Numbers); debugg purpose
         
        }//end of each case
    }
    
    private static int[] getInput(String filename){
        int[] cases=new int[100];
        ArrayList<String> content=new ArrayList<>();
        
        try (BufferedReader br = new BufferedReader(new FileReader(filename)))
		{

			String sCurrentLine;

			while ((sCurrentLine = br.readLine()) != null) {
				content.add(sCurrentLine);
                            //System.out.println(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
        
        /*
        The first line of the input gives the number of test cases, T. 
        T test cases follow. Each consists of one line with a single integer N,
        the number Bleatrix has chosen.
        */
        cases=new int[Integer.parseInt(content.get(0))];
        for(int i=0;i<cases.length;i++){
            cases[i]=Integer.parseInt(content.get(i+1));
        }
        
        return cases;
    }
    
    private static void showNumbers(ArrayList<Integer> N){
        for(int i=0;i<N.size();i++)
        System.out.print(" "+i+":-"+N.get(i));
        System.out.println();
    }
}
